package main

import (
	"fmt"
	"os"

	cli "github.com/jawher/mow.cli"
	"github.com/sirupsen/logrus"
	"gitlab.dnm.radiofrance.fr/ceo/gitlab-cli/cmd"
)

const (
	gitlabCliVersion = "unreleased"
)

// nolint:gochecknoinits
// @see: https://github.com/sirupsen/logrus#environments
func init() {
	// Set logger loglevel to DEBUG if env var "DEBUG_MODE" are present
	_, debugMode := os.LookupEnv("DEBUG_MODE")
	if debugMode {
		logrus.SetLevel(logrus.DebugLevel)
	}
}

func main() {
	app := cli.App("gitlab-cli", "A cli to perform operations on Gitlab instance")

	app.Command("user", "Perform action on Gitlab users", func(user *cli.Cmd) {
		user.Command("deactivate", "Disables all users that are inactive since at least 90 days", cmd.DeactivateUser)
	})
	app.Command("-v --version version", "Display version", cmdGetVersion)

	if err := app.Run(os.Args); err != nil {
		logrus.Fatalf("Failed to run: %v", err)
	}
}

// cmdGetVersion print the current version of the project and then exit
func cmdGetVersion(cmd *cli.Cmd) {
	cmd.Action = func() {
		fmt.Printf("gitlab-cli has version %s\n", gitlabCliVersion)
	}
}
