package cmd

import (
	"strings"
	"time"

	cli "github.com/jawher/mow.cli"
	"github.com/sirupsen/logrus"
	"github.com/xanzy/go-gitlab"
	"gitlab.dnm.radiofrance.fr/ceo/gitlab-cli/commons"
)

const maximalDaysOfInactivity = 90

// DeactivateUser wrap the sub command "user deactivate".
func DeactivateUser(cmd *cli.Cmd) {
	opts := commons.UserDeactivateOptions{}

	cmd.Spec = "[--no-dry-run] [--ignore-users=<email,email>]"
	cmd.BoolOptPtr(&opts.NoDryRun, "no-dry-run", false, "Command run by default on dry-run mode and ONLY list user eligible to deactivate.\nPass these flag to really deactivating them.")
	cmd.StringOptPtr(&opts.IgnoredUsers, "i ignore-users", "", "A list of user email to exclude, separated by commas\nex: gitlab-cli user deactivate -i \"admin@gitlab.com,root@example.com\"")

	cmd.Action = func() {
		logrus.Info("Deactivating inactive Gitlab users")
		if !opts.NoDryRun {
			logrus.Warn("flag \"--no-dry-run\" is not set, running in dry-run mode")
		}

		if opts.IgnoredUsers != "" {
			logrus.Debugf("Ignored users: %s", opts.IgnoredUsers)
		}

		cfg := commons.GetConfig()
		disabledUsers, err := deactivateAction(cfg, &opts)
		if err != nil {
			logrus.Fatal(err)
		}

		if disabledUsers == 0 {
			logrus.Info("There are no users to deactivate")
			return
		}

		var deactivatingMessage string
		if opts.NoDryRun {
			deactivatingMessage = "%d users have been deactivated"
			if disabledUsers == 1 {
				deactivatingMessage = "%d user has been deactivated"
			}
		} else {
			deactivatingMessage = "%d users will be deactivated, pass \"--no-dry-run\" flag to really deactivate them"
			if disabledUsers == 1 {
				deactivatingMessage = "%d user will be deactivated, pass \"--no-dry-run\" flag to really deactivate them"
			}
		}

		logrus.Infof(deactivatingMessage, disabledUsers)
	}
}

// deactivateAction implements the "user deactivate" command and handles all the logic for
// querying the Gitlab API to retrieve the list of users, checking their last activity and deactivate
// them if inactive. It return a counter of deactivated users if it succeed, an error if not.
func deactivateAction(config *commons.Config, opts *commons.UserDeactivateOptions) (int, error) {
	logrus.Info("Retrieve list of Gitlab users ...")
	git, err := gitlab.NewClient(config.GitlabToken, gitlab.WithBaseURL(config.GitlabBaseURL))
	if err != nil {
		return 0, err
	}

	// Fetch list of Gitlab users
	listUsersOptions := gitlab.ListUsersOptions{
		Active:  gitlab.Bool(true),
		Blocked: gitlab.Bool(false),
		Admins:  gitlab.Bool(false),
	}

	usersList, req, err := git.Users.ListUsers(&listUsersOptions)
	if err != nil {
		return 0, err
	}

	for req.CurrentPage = 1; req.CurrentPage <= req.TotalPages; req.CurrentPage++ {
		logrus.Debugf("%d/%d users retrieved", len(usersList), req.TotalItems)
		listUsersOptions.ListOptions.Page = req.NextPage

		var users []*gitlab.User
		users, req, err = git.Users.ListUsers(&listUsersOptions)
		if err != nil {
			return 0, err
		}

		usersList = append(usersList, users...)
	}

	logrus.Infof("%d users retrieved, check if any of them is eligible to deactivation ...", len(usersList))

	deactivatedUsers := deactivateInactiveUsers(git, opts, usersList)

	return deactivatedUsers, nil
}

// deactivateInactiveUsers Take a list of gitlab users and check last activities for each of them
// and deactivate it if required. It return a counter of deactivated user
func deactivateInactiveUsers(gitlab *gitlab.Client, opts *commons.UserDeactivateOptions, usersList []*gitlab.User) int {
	deactivatedUsers := 0
	for _, user := range usersList {
		if isIgnored(opts.IgnoredUsers, user.Email) {
			continue
		}

		if hasNoActivitySince90Days(user.LastActivityOn, user.CurrentSignInAt) {
			if opts.NoDryRun {
				err := gitlab.Users.DeactivateUser(user.ID)
				if err != nil {
					logrus.Errorf("error when trying to deativate user no°%d %s, message: %v", user.ID, user.Email, err)
					continue
				}

				logrus.Infof("User %s with id %d has been deactivated", user.Email, user.ID)
				deactivatedUsers++

				continue
			}

			logrus.Infof("User %s with id %d is eligible to deactivation", user.Email, user.ID)
			deactivatedUsers++
		}
	}

	return deactivatedUsers
}

// hasNoActivitySince90Days function check the most recent date between given lastActivity & currentSignInAt dates
// and calculate the days passed until now. It return true if calculated value is superior to maximalDaysOfInactivity,
// false instead
func hasNoActivitySince90Days(lastActivity *gitlab.ISOTime, currentSignInAt *time.Time) bool {
	hoursInADay := 24.0
	now := time.Now()
	youngestDate := now

	if currentSignInAt != nil {
		youngestDate = *currentSignInAt
	}

	if lastActivity != nil {
		activityTime := time.Time(*lastActivity)
		if currentSignInAt != nil && activityTime.Unix() > currentSignInAt.Unix() {
			youngestDate = activityTime
		}
	}

	daysPassed := int(now.Sub(youngestDate).Hours() / hoursInADay)

	return daysPassed > maximalDaysOfInactivity
}

// isIgnored function check if given user email is present in given ignoredUsersList
// it return true is present, false if not present or if ignoredUsersList is empty
func isIgnored(ignoredUsersList string, email string) bool {
	if ignoredUsersList == "" {
		return false
	}

	return strings.Contains(ignoredUsersList, email)
}
