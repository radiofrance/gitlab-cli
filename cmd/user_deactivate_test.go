package cmd

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
	"github.com/xanzy/go-gitlab"
)

func Test_hasNoActivitySince90Days(t *testing.T) {
	t.Parallel()
	now := time.Now()

	tests := []struct {
		name            string
		lastActivity    time.Time
		currentSignInAt time.Time
		expected        bool
	}{
		{"no activity since 1 year", now.AddDate(-1, 0, 0), now.AddDate(-1, 0, 0), true},
		{"no activity since 4 months", now.AddDate(0, -4, 0), now.AddDate(0, -4, 0), true},
		{"no activity since 3 months and 2 days", now.AddDate(0, -3, -2), now.AddDate(0, -3, -2), true},
		{"no activity since 90 days", now.AddDate(0, 0, -90), now.AddDate(0, 0, -90), false},
		{"no activity since 1 month", now.AddDate(0, -1, 0), now.AddDate(0, -1, 0), false},
		{"Current day", now, now, false},
		{"1 day in future", now.AddDate(0, 0, 1), now.AddDate(0, 0, 1), false},
		{"4 months since last activity and recent sign in", now.AddDate(0, -4, 0), now.AddDate(0, 0, -3), false},
		{"recent last activity and 4 month passed since last sign in", now.AddDate(0, 0, -2), now.AddDate(0, -4, 0), false},
	}

	for _, test := range tests {
		test := test
		lastActivity := gitlab.ISOTime(test.lastActivity)

		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			actual := hasNoActivitySince90Days(&lastActivity, &test.currentSignInAt)
			assert.Equal(t, test.expected, actual)
		})
	}

	t.Run("test nil values", func(t *testing.T) {
		t.Parallel()
		nullTestResult := hasNoActivitySince90Days(nil, nil)
		assert.Equal(t, false, nullTestResult)
	})
}

func Test_isIgnored(t *testing.T) {
	t.Parallel()

	ignoreList := "system@xxxxxx.y,admin@example.com,hello-world@hello.world,user@gitlab.com,john.smith,brainless@verizon.net,philb@me.com,jbearp@icloud.com"

	tests := []struct {
		name     string
		email    string
		expected bool
	}{
		{"Present", "user@gitlab.com", true},
		{"Present2", "john.smith", true},
		{"Not present", "unknown@example.com", false},
		{"Not present2", "jimmichie2@gmail.com", false},
	}

	for _, test := range tests {
		test := test
		t.Run(test.name, func(t *testing.T) {
			t.Parallel()
			actual := isIgnored(ignoreList, test.email)
			assert.Equal(t, test.expected, actual)
		})
	}

	t.Run("empty ignoreList", func(t *testing.T) {
		t.Parallel()
		actual := isIgnored("", "user@gitlab.com")
		assert.Equal(t, false, actual)
	})
}
