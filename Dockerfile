FROM golang:1.16 as builder

ENV CGO_ENABLED=0
ENV GOARCH=amd64
ENV GOOS=linux

COPY . /src

RUN cd /src \
    && go build -a -installsuffix cgo -ldflags '-extldflags "-static"' -o gitlab-cli .

FROM debian:buster

RUN set -x \
    && apt-get update -y \
    && apt-get install -y ca-certificates \
    && rm -rf /var/lib/apt/lists/* \
    && groupadd --gid 1664 gopher \
    && useradd --comment 'Go user' --uid 1664 --gid 1664 --shell /bin/sh gopher

COPY --from=builder /src/gitlab-cli /usr/local/bin/gitlab-cli

RUN chmod +x /usr/local/bin/gitlab-cli

USER gopher

ENTRYPOINT [ "/usr/local/bin/gitlab-cli" ]
