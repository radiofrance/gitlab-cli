package commons

// Config struct contain all configuration of the app
type Config struct {
	GitlabBaseURL string `envconfig:"GITLAB_BASE_URL" required:"true"`
	GitlabToken   string `envconfig:"GITLAB_TOKEN" required:"true"`
}

// UserDeactivateOptions represent all the options we able to pass to "user deactivate" command
type UserDeactivateOptions struct {
	NoDryRun     bool
	IgnoredUsers string
}
