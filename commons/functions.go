package commons

import (
	"github.com/kelseyhightower/envconfig"
	"github.com/sirupsen/logrus"
)

// GetConfig create an instance of Config, then populate her values from environment variable.
// It return a Config instance if it succeed or throw an error and exit if not.
func GetConfig() *Config {
	var cfg Config
	err := envconfig.Process("", &cfg)
	if err != nil {
		logrus.Fatal(err)
	}

	return &cfg
}
