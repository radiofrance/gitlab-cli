module gitlab.dnm.radiofrance.fr/ceo/gitlab-cli

go 1.16

require (
	github.com/jawher/mow.cli v1.2.0
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/sirupsen/logrus v1.8.1
	github.com/stretchr/testify v1.7.0
	github.com/xanzy/go-gitlab v0.52.2
	golang.org/x/sys v0.0.0-20210228012217-479acdf4ea46 // indirect
)
