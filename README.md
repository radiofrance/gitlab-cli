## Gitlab CLI

### Description

"gitlab-cli" is a tiny util program written in Go.

Its main purpose is to help administrators of custom Gitlab instance to perform complex maintenance operation.

### Usages

To run properly, you need to set the following variables:
- `GITLAB_BASE_URL` --> The base url of your Gitlab instance, without trailing slash at the end, ex: "https://gitlab.com"
- `GITLAB_TOKEN` --> A admin user personal access token with scopes "`api`" (admin user **is required** to perform some actions, like deactivating a user)

example using the provided Docker image:
```
export GITLAB_BASE_URL="xxxx"
export GITLAB_TOKEN="xxxx"

docker run -ti --rm \
    -e GITLAB_BASE_URL=$GITLAB_BASE_URL \
    -e GITLAB_TOKEN=$GITLAB_TOKEN \
    gitlab.com/radiofrance/gitlab-cli:latest
    {command to run}
```

List of available commands:

- [`user deactivate`](#cmd-user-deactivate)

---

##### <a id="cmd-user-deactivate"></a> User Deactivate

```
Usage: gitlab-cli user deactivate [--no-dry-run] [--ignore-users=<email,email>]

Disables all users that are inactive since at least 90 days
                       
Options:               
      --no-dry-run     Command run by default on dry-run mode and ONLY list user eligible to deactivate.
                       Pass these flag to really deactivating them.
  -i, --ignore-users   A list of user email to exclude, separated by commas
                       ex: gitlab-cli user deactivate -i "admin@gitlab.com,root@example.com"
```

List and deactivate all inactive Gitlab users to free licences seat.

A user is considered as "inactive", if they won't have any activity since 90 days
Deactivating a user is identical to blocking a user, but does not prohibit the user from logging back in via the UI. Once a deactivated user logs back into the GitLab UI, their account is set to active.
Inactive users do not take up a licences seat.

Related documentation: 
- https://docs.gitlab.com/ee/api/users.html#deactivate-user
- https://docs.gitlab.com/ee/user/admin_area/activating_deactivating_users.html#deactivating-a-user

:warning: **Require a Gitlab instance with at least version `12.4` to work**

---
